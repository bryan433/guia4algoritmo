#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "arbol.h"


using namespace std;


int main (void){
	
	int opcion;
	int n;
	int Delet;
	arbol *Arbol = new arbol();
	
	Nodo *grafo = NULL;
	
	while (true){
		
		cout << "Insertar un numero: [1]" << endl;
		cout << "Eliminar numero buscado: [2]" << endl;
		cout << "Modificar un elemnto buscado: [3]" << endl;
		cout << "Generar el arbol graficamente: [4]" << endl;
		cout << "Mostrar el arbol en Preorden: [5]" << endl;
		cout << "Mostrar el arbol en Inorden: [6]" << endl;
		cout << "Mostrar el arbol en Posorden: [7]" << endl;
		cout << "Salir: [8]" << endl;
		cin >> opcion;
		
		switch (opcion){
		
			case 1:{
				system("clear");
				cout << "ingrese un numero para el arbol: " << endl;
				cin >> n;
				
				if (Arbol->busqueda(grafo, n) == true){
					cout << "El elemento " << n << "se encuentra en el arbol" << endl;
				}
				else{
					cout << "El elemento " << n << " no se encuentra en el arbol" << endl;
					cout << "se agrego al arbol" << endl;
					Arbol->insertarNodo(grafo, n, NULL);
				}
				break;
			}
			case 2:{
				system("clear");
				cout << "ingrese el numero a eliminar del arbol: " << endl;
				cin >> Delet;	
				
				if (Arbol->busqueda(grafo, Delet) == true){
					cout << "El elemento " << Delet << "se encuentra en el arbol" << endl;
					Arbol->eliminar(grafo, Delet);
				}
				else{
					cout << "El elemento" << Delet << "no se encuentra en el arbol" << endl;
				}
				break;
			}
			case 3:{
				system("clear");
				cout << "Se modifico el elemento" << endl;
				break;
			}
			case 4:{
				system("clear");
				cout << "Se genero el grafo" << endl;
				Arbol->crearGrafo(grafo);
				break;
			}
			case 5:{
				system("clear");
				cout << "-Preorden-\n" << endl;
				Arbol->preOrden(grafo);
				break;
			}
			case 6:{
				system("clear");
				cout << "-Inorden-" << endl;
				Arbol->inOrden(grafo);
				break;
			}
			case 7:{
				system("clear");
				cout << "-Posorden-" << endl;
				Arbol->postOrden(grafo);
				break;
			}
			case 8:{
				system("clear");
				exit (0);
				break;
			}
			system("clear");
		}
	
	}
	return 0;
}
