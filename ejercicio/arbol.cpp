#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include "arbol.h"

using namespace std;

arbol::arbol(){	
}


Nodo *crearNodo(int n, Nodo *padre){
	
	Nodo *raiz;
	
	raiz = new Nodo();
	
	raiz->der = NULL;
	raiz->izq = NULL;
	raiz->info = n;
	raiz->padre = padre;
	
	return raiz;
}


void arbol::recorrerArbol(Nodo *puntero, ofstream &archivo){

	string infoTmp;
	
	if (puntero != NULL){
	
		if (puntero->izq != NULL){
			archivo << puntero->info << "->" << puntero->izq->info << ";" << endl;
		}
		else{
			infoTmp = to_string(puntero->info) + "i";
			infoTmp = "\"" + infoTmp + "\"";
			archivo << infoTmp << "[shape=point]" << endl;
			archivo << puntero->info << "->" << infoTmp << ";" << endl;
		}
		infoTmp = puntero->info;
		
		if (puntero->der != NULL){
			archivo << puntero->info << "->" << puntero->der->info << ";" << endl;
		}
		else{
			infoTmp = to_string(puntero->info) + "d";
			infoTmp = "\"" + infoTmp + "\"";
			archivo << infoTmp << "[shape=point]" << endl;
			archivo << puntero->info << "->" << infoTmp << ";" << endl;
		}
		recorrerArbol(puntero->izq, archivo);
		recorrerArbol(puntero->der, archivo);
	}
	return;
}

void arbol::crearGrafo(Nodo *raiz){
	
	ofstream archivo;
	archivo.open("datos.txt");
	archivo << "digraph G {" << endl;
	archivo << "node [style=filled fillcolor=yellow];" << endl;
	recorrerArbol(raiz, archivo);
	archivo << "}" << endl;
	archivo.close();
	
	system("dot -Tpng -ografo.png datos.txt &");
}

void arbol::insertarNodo(Nodo *&grafo, int n, Nodo *padre){
	if (grafo == NULL){
		Nodo *nodo = crearNodo(n, padre);
		grafo = nodo;
		
	}
	else{
		int valorinicial = grafo->info;
		if (n < valorinicial){
			insertarNodo(grafo->izq, n, grafo);
		}
		else{
			insertarNodo(grafo->der, n, grafo);
		}
	}
}

bool arbol::busqueda (Nodo *grafo, int n){
	if (grafo == NULL){
		return false;
	}
	else if (grafo->info == n){
		return true;
	}
	else if (n < grafo->info){
		return busqueda(grafo->izq, n);
	}
	else{
		return busqueda(grafo->der, n);
	}
}

void arbol::preOrden(Nodo *grafo){
	if (grafo == NULL){
		return;
	}
	else{
		cout << grafo->info << " - ";
		preOrden(grafo->izq);
		preOrden(grafo->der);
	}
}

void arbol::inOrden(Nodo *grafo){
	if (grafo == NULL){
		return;
	}
	else{
		inOrden(grafo->izq);
		cout << grafo->info << " - ";
		inOrden(grafo->der);
	}
}

void arbol::postOrden(Nodo *grafo){
	if(grafo == NULL){
		return;
	}
	else{
		postOrden(grafo->izq);
		postOrden(grafo->der);
		cout << grafo->info << " - ";
	}
}

Nodo *minimo(Nodo *grafo){
	if (grafo == NULL){
		return NULL;
	}
	if (grafo->izq){
		return minimo(grafo->izq);
	}
	else{
		return grafo;
	}
}

void arbol::reemplazar(Nodo *grafo, Nodo *nuevoNodo){
	if (grafo->padre){
		if (grafo->info == grafo->padre->izq->info){
			grafo->padre->izq = nuevoNodo;
		}
		else if (grafo->info == grafo->padre->der->info){
			grafo->padre->der = nuevoNodo;
		}
	}
	if (nuevoNodo){
		nuevoNodo->padre = grafo->padre;
	}
}

void arbol::destruirNodo(Nodo *nodo){
	nodo->izq = NULL;
	nodo->der = NULL;
	
	delete nodo;
}

void arbol::eliminarNodo(Nodo *nodoDelet){
	if (nodoDelet->izq && nodoDelet->der){
		Nodo *menor = minimo(nodoDelet->der);
		nodoDelet->info = menor->info;
		eliminarNodo(menor);
	}
	else if (nodoDelet->izq){
		reemplazar(nodoDelet, nodoDelet->izq);
		destruirNodo(nodoDelet);
	}
	else if (nodoDelet->der){
		reemplazar(nodoDelet, nodoDelet->der);
		destruirNodo(nodoDelet);
	}
	else{
		reemplazar(nodoDelet, NULL);
		destruirNodo(nodoDelet);
	}
}

void arbol::eliminar (Nodo *grafo, int n){
	if (grafo == NULL){
		return;
	}
	else if (n < grafo->info){
		eliminar(grafo->izq, n);
	}
	else if (n > grafo->info){
		eliminar(grafo->der, n);
	}
	else{
		eliminarNodo(grafo);
	}
}
