-Empezando el programa: al inicalizar el programa nos saldra un menu la primera opcion es para ir añadiendo numeros y creando nodos a nuestro arbol binario, la segunda opcion, elimina el numero que nosotros queremos eliminar, primero debe buscarlo y saber si se encuentra y luego eliminarlo, la tercera opcion nos ayuda a modificar algun Nodo del arbol, la cuarta opcion nos genera un grafico en formato png, el cual si entramos a la carpeta donde esta todo nuestro programa encontraremos ese png, la quinta opcion nos mostrara el arbol en preorden, la sexta opcion nos mostrara el arbol en inorden, la septima opcion nos mostrara el arbol en posorden y la ultima opcion nos permitira salir del programa.

-Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/guia4algoritmo clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

-Sistema operativo:
Debian GNU/Linux 10 (buster)

-Autor:
Bryan Ahumada Ibarra
