#include <iostream>
#include <fstream>

using namespace std;

typedef struct _Nodo {
    int info;
    struct _Nodo *izq;
    struct _Nodo *der;
    struct _Nodo *padre;
} Nodo;


class arbol{
	private:
	
	public:
		arbol();
		void crearGrafo(Nodo *raiz);
		void recorrerArbol(Nodo *puntero, ofstream &archivo);
		void insertarNodo(Nodo *&grafo, int n, Nodo *padre);
		bool busqueda (Nodo *grafo, int n);
		void preOrden(Nodo *grafo);
		void inOrden(Nodo *grafo);
		void postOrden(Nodo *grafo);
		void eliminarNodo(Nodo *grafo);
		void eliminar (Nodo *grafo, int n);
		void reemplazar(Nodo *grafo, Nodo *nuevoNodo);
		void destruirNodo(Nodo *nodo);


};
